#include "model.h"
#include <QtOpenGL>
typedef float vec4[4];
Model::Model()
{
    m_modeDraw = Triangles;
}

Model::Model(const QString& filename)
{
    m_modeDraw = Triangles;
    load(filename);
}
void Model::load(const QString& filename)
{
    m_model = new DeserializationObj;
    m_model->load(filename);
    //определение файлов текстур для последующей загрузки
    foreach(DeserializationMtl * fileMtl, m_model->store_Mtl)
    {
        foreach(Material * material, fileMtl->store_material)
        {
            if (!material->diffuse_texture_name.isEmpty()) {
                m_store_textures.insert(material->diffuse_texture_name, 0);
            }
        }
    }
    qDebug() << "loading model";
}


Model::ModeDraw Model::modeDraw() const
{
    return m_modeDraw;
}

void Model::setModeDraw(const ModeDraw& modeDraw)
{
    m_modeDraw = modeDraw;
}

QMatrix4x4 Model::modelTransform() const
{
    return m_modelTransform;
}

void Model::setModelTransform(const QMatrix4x4& modelTransform)
{
    m_modelTransform = modelTransform;
}

bool Model::useMaterial(const QString& name)
{
    Material* material = m_model->material(name);
    if (material != 0) {
        useMaterial(material);
        return true;
    } else {
        //qDebug() << "not find material:" << name;
        return false;
    }
}

void Model::useMaterial(Material* material)
{
    vec4 ambient, diffuse, specular, emission;
    float shininess;
    float dissolve = material->dissolve;

    ambient[0] = material->ambient.x();
    ambient[1] = material->ambient.y();
    ambient[2] = material->ambient.z();
    ambient[3] = material->ambient.w();

    diffuse[0] = material->diffuse.x();
    diffuse[1] = material->diffuse.y();
    diffuse[2] = material->diffuse.z();
    diffuse[3] = material->diffuse.w();

    specular[0] = material->specular.x();
    specular[1] = material->specular.y();
    specular[2] = material->specular.z();
    specular[3] = material->specular.w();

    emission[0] = material->emission.x();
    emission[1] = material->emission.y();
    emission[2] = material->emission.z();
    emission[3] = material->emission.w();

    shininess = material->shininess;

    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, emission);
    glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, &shininess);

    if (m_store_textures.contains(material->diffuse_texture_name)) {
        GLuint texture = m_store_textures.value(material->diffuse_texture_name);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
}

void Model::draw(const QMatrix4x4& world)
{
    //переводим в мировые координаты
    QMatrix4x4 normal = world;
    normal.normalMatrix();
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(world.data());
    glBegin(GL_TRIANGLES);
    foreach(PrepareShape * shape, m_model->store_shape)
    {
        //устанавливаем материал
        if (useMaterial(shape->material)){
            for (int i = 0; i<shape->position.length(); ++i){
                QVector3D vec = shape->position[i];
                glVertex3f(vec.x(), vec.y(), vec.z());
            }
            if (shape->isUseNormal) {
                for (int i = 0; i<shape->normal.length(); ++i){
                    QVector3D norm = shape->normal[i];
                    glVertex3f(norm.x(), norm.y(), norm.z());
                }
            }
        }
    }
    glEnd();
}

void Model::draw()
{
    draw(m_modelTransform);
}
