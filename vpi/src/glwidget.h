#ifndef GLWIDGET_H
#define GLWIDGET_H
#include <QtOpenGL>
#include <QVector3D>
#include <QMatrix4x4>
#include <QTime>
#include <QVector>
#include <QPair>
#include <memory>
#include "model.h"

float random_range(float, float);
class GLWidget : public QGLWidget {
    Q_OBJECT
public:
    GLWidget(QWidget* parent = 0);
    ~GLWidget();
    void prepare();
protected:
    void paintGL();
    void initializeGL();
    void resizeGL(int width, int height);
    void keyPressEvent(QKeyEvent* event);
    void animate();
private:
    float m_aspectRatio;//отножение ширины к высоте
    QVector3D m_position;//позиция камеры
    QVector3D m_center;//точка взгляда
    QTimer* m_timer;
    QMatrix4x4 m_modelTransform;
    QMatrix4x4 m_viewTransform;
    QMatrix4x4 m_projection;
    QMatrix4x4 m_view;
    std::unique_ptr<Model> m_model_scene_vpi;
    std::vector<std::unique_ptr<Model>> m_model_tree;
    float m_new_wind;
    float m_end_wind;
    float m_step_wind;
    float m_new_force_wind;
    float m_end_force_wind;
    float m_step_force_wind;
    std::vector< QPair <int, QVector3D> > m_position_tree;
    float m_angle_sun;
    int m_animate_state;
    int m_animate_states;
    bool m_is_animate;
};

#endif
