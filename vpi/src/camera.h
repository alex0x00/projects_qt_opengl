#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QVector3D>

class Camera : public QObject {
    Q_OBJECT
public:
    Camera(QObject* parent = 0);
    ~Camera()
    {
    }

    inline void updatePerpectiveProjection()
    {
        m_projectionMatrix.setToIdentity();
        m_projectionMatrix.perspective(m_fieldOfView, m_aspectRatio, m_nearPlane, m_farPlane);
        m_viewProjectionMatrixDirty = true;
    }

    inline void updateOrthogonalProjection()
    {
        m_projectionMatrix.setToIdentity();
        m_projectionMatrix.ortho(m_left, m_right, m_bottom, m_top, m_nearPlane, m_farPlane);
        m_viewProjectionMatrixDirty = true;
    }

    enum ProjectionType {
        OrthogonalProjection,
        PerspectiveProjection
    };

    enum CameraTranslationOption {
        TranslateViewCenter,
        DontTranslateViewCenter
    };

    QVector3D position() const;
    QVector3D upVector() const;
    QVector3D viewCenter() const;

    QVector3D viewVector() const;

    ProjectionType projectionType() const;

    void setOrthographicProjection(float left, float right,
                                   float bottom, float top,
                                   float nearPlane, float farPlane);

    void setPerspectiveProjection(float fieldOfView, float aspect,
                                  float nearPlane, float farPlane);

    void setNearPlane(const float& nearPlane);
    float nearPlane() const;

    void setFarPlane(const float& nearPlane);
    float farPlane() const;

    void setFieldOfView(const float& fieldOfView);
    float fieldOfView() const;

    void setAspectRatio(const float& aspectRatio);
    float aspectRatio() const;

    void setLeft(const float& left);
    float left() const;

    void setRight(const float& right);
    float right() const;

    void setBottom(const float& bottom);
    float bottom() const;

    void setTop(const float& top);
    float top() const;

    void setPosition( const QVector3D& position );
    void setUpVector( const QVector3D& upVector );
    void setViewCenter( const QVector3D& viewCenter );

    QMatrix4x4 viewMatrix() const;
    QMatrix4x4 projectionMatrix() const;
    QMatrix4x4 viewProjectionMatrix() const;

private:
    QVector3D m_position;
    QVector3D m_upVector;
    QVector3D m_viewCenter;
    QVector3D m_cameraToCenter;

    Camera::ProjectionType m_projectionType;

    float m_nearPlane;
    float m_farPlane;

    float m_fieldOfView;
    float m_aspectRatio;

    float m_left;
    float m_right;
    float m_bottom;
    float m_top;

    mutable QMatrix4x4 m_viewMatrix;
    mutable QMatrix4x4 m_projectionMatrix;
    mutable QMatrix4x4 m_viewProjectionMatrix;

    mutable bool m_viewMatrixDirty;
    mutable bool m_viewProjectionMatrixDirty;
};

#endif // CAMERA_H
