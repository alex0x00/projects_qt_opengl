#include "camera.h"

Camera::Camera(QObject* parent)
    : QObject(parent)
    , m_position(0.0f, 0.0f, 1.0f)
    , m_upVector(0.0f, 1.0f, 0.0f)
    , m_viewCenter(0.0f, 0.0f, 0.0f)
    , m_cameraToCenter(0.0f, 0.0f, -1.0f)
    , m_projectionType(Camera::OrthogonalProjection)
    , m_nearPlane(0.1f)
    , m_farPlane(1024.0f)
    , m_fieldOfView(60.0f)
    , m_aspectRatio(1.0f)
    , m_left(-0.5)
    , m_right(0.5f)
    , m_bottom(-0.5f)
    , m_top(0.5f)
    , m_viewMatrixDirty(true)
    , m_viewProjectionMatrixDirty(true)
{
}

Camera::ProjectionType Camera::projectionType() const
{
    return m_projectionType;
}

QVector3D Camera::position() const
{
    return m_position;
}

void Camera::setPosition(const QVector3D& position)
{
    m_position = position;
    m_cameraToCenter = m_viewCenter - position;
    m_viewMatrixDirty = true;
}

void Camera::setUpVector(const QVector3D& upVector)
{
    m_upVector = upVector;
    m_viewMatrixDirty = true;
}

QVector3D Camera::upVector() const
{

    return m_upVector;
}

void Camera::setViewCenter(const QVector3D& viewCenter)
{
    m_viewCenter = viewCenter;
    m_cameraToCenter = viewCenter - m_position;
    m_viewMatrixDirty = true;
}

QVector3D Camera::viewCenter() const
{
    return m_viewCenter;
}

QVector3D Camera::viewVector() const
{
    return m_cameraToCenter;
}

void Camera::setOrthographicProjection(float left, float right,
                                       float bottom, float top,
                                       float nearPlane, float farPlane)
{
    m_left = left;
    m_right = right;
    m_bottom = bottom;
    m_top = top;
    m_nearPlane = nearPlane;
    m_farPlane = farPlane;
    m_projectionType = OrthogonalProjection;
    updateOrthogonalProjection();
}

void Camera::setPerspectiveProjection(float fieldOfView, float aspectRatio,
                                      float nearPlane, float farPlane)
{
    m_fieldOfView = fieldOfView;
    m_aspectRatio = aspectRatio;
    m_nearPlane = nearPlane;
    m_farPlane = farPlane;
    m_projectionType = PerspectiveProjection;
    updatePerpectiveProjection();
}

void Camera::setNearPlane(const float& nearPlane)
{
    if (qFuzzyCompare(m_nearPlane, nearPlane))
        return;
    m_nearPlane = nearPlane;
    if (m_projectionType == PerspectiveProjection)
        updatePerpectiveProjection();
}

float Camera::nearPlane() const
{
    return m_nearPlane;
}

void Camera::setFarPlane(const float& farPlane)
{
    if (qFuzzyCompare(m_farPlane, farPlane))
        return;
    m_farPlane = farPlane;
    if (m_projectionType == PerspectiveProjection)
        updatePerpectiveProjection();
}

float Camera::farPlane() const
{
    return m_farPlane;
}

void Camera::setFieldOfView(const float& fieldOfView)
{
    if (qFuzzyCompare(m_fieldOfView, fieldOfView))
        return;
    m_fieldOfView = fieldOfView;
    if (m_projectionType == PerspectiveProjection)
        updatePerpectiveProjection();
}

float Camera::fieldOfView() const
{
    return m_fieldOfView;
}

void Camera::setAspectRatio(const float& aspectRatio)
{
    if (qFuzzyCompare(m_aspectRatio, aspectRatio))
        return;
    m_aspectRatio = aspectRatio;
    if (m_projectionType == PerspectiveProjection)
        updatePerpectiveProjection();
}

float Camera::aspectRatio() const
{
    return m_aspectRatio;
}

void Camera::setLeft(const float& left)
{
    if (qFuzzyCompare(m_left, left))
        return;
    m_left = left;
    if (m_projectionType == OrthogonalProjection)
        updateOrthogonalProjection();
}

float Camera::left() const
{
    return m_left;
}

void Camera::setRight(const float& right)
{
    if (qFuzzyCompare(m_right, right))
        return;
    m_right = right;
    if (m_projectionType == OrthogonalProjection)
        updateOrthogonalProjection();
}

float Camera::right() const
{
    return m_right;
}

void Camera::setBottom(const float& bottom)
{
    if (qFuzzyCompare(m_bottom, bottom))
        return;
    m_bottom = bottom;
    if (m_projectionType == OrthogonalProjection)
        updateOrthogonalProjection();
}

float Camera::bottom() const
{
    return m_bottom;
}

void Camera::setTop(const float& top)
{
    if (qFuzzyCompare(m_top, top))
        return;
    m_top = top;
    if (m_projectionType == OrthogonalProjection)
        updateOrthogonalProjection();
}

float Camera::top() const
{
    return m_top;
}

QMatrix4x4 Camera::viewMatrix() const
{
    if (m_viewMatrixDirty) {
        m_viewMatrix.setToIdentity();
        m_viewMatrix.lookAt(m_position, m_viewCenter, m_upVector);
        m_viewMatrixDirty = false;
    }
    return m_viewMatrix;
}

QMatrix4x4 Camera::projectionMatrix() const
{
    return m_projectionMatrix;
}

QMatrix4x4 Camera::viewProjectionMatrix() const
{
    if (m_viewMatrixDirty || m_viewProjectionMatrixDirty) {
        m_viewProjectionMatrix = m_projectionMatrix * viewMatrix();
        m_viewProjectionMatrixDirty = false;
    }
    return m_viewProjectionMatrix;
}

