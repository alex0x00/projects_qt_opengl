#include "glwidget.h"
#include <QDebug>
#include <QImage>
#include <QList>
#include <QtCore>

const QString PATH_SRC = ":/";
const QString PATH_SRC_SCENE = ":/vpi.obj";
const QString PATH_SRC_TREE_3 = ":/tree.obj";
const QString PATH_SRC_TREE_1 = ":/tree1.obj";
const QString PATH_SRC_TREE_2 = ":/tree2.obj";

float random_range(float min = 0.0, float max = 1.0)
{
    float q = qrand() / (float)RAND_MAX;
    return (max - min) * q + min;
}
GLWidget::GLWidget(QWidget* parent)
    : QGLWidget(parent)
{
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_NoSystemBackground);
    setAutoBufferSwap(false);
    m_angle_sun = 0.0;
    m_position = QVector3D(0, 5, 50);
    m_center = QVector3D(0, 5, 0);
    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(4);
    setFormat(fmt);
    m_animate_state = 0;
    m_animate_states = 1000;
    m_new_wind = 60.0;
    m_end_wind = 0;
    m_step_wind = (m_new_wind - m_end_wind) / m_animate_states;

    m_new_force_wind = 20.0;
    m_end_force_wind = 0;
    m_step_force_wind = (m_new_force_wind - m_end_force_wind) / m_animate_states;
    m_is_animate = true;
    m_timer = new QTimer(this);
    QObject::connect(m_timer, SIGNAL(timeout()), this, SLOT(updateGL()));
    m_timer->setInterval(100);
    m_timer->start();
    prepare();
}

void GLWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
    m_aspectRatio = width / height;
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    float step = 1.0f;
    //выходи по escape
    //вращение модели
    switch (event->key()) {
    case Qt::Key_Escape:
        QApplication::quit();
        break;
    case Qt::Key_Left:
        m_viewTransform.rotate(step, QVector3D(0, 1, 0));
        break;
    case Qt::Key_Right:
        m_viewTransform.rotate(-step, QVector3D(0, 1, 0));
        break;
    case Qt::Key_Up: //вверх
        m_position += QVector3D(0.0, step, 0.0);
        m_center += QVector3D(0.0, step, 0.0);
        break;
    case Qt::Key_Down: //вниз
        m_position += QVector3D(0.0, -step, 0.0);
        m_center += QVector3D(0.0, -step, 0.0);
        break;
    case Qt::Key_Plus: { //приближение
        QVector3D direction = m_position - m_center;
        float dist = direction.length() - step > 1.0 ? step : 1.0;
        direction.normalize();
        m_position -= direction * dist;
        break;
    }
    case Qt::Key_Minus: { //отдаление
        QVector3D direction = m_position - m_center;
        float dist = direction.length() - step > 1.0 ? step : 1.0;
        direction.normalize();
        m_position += direction * dist;
        break;
    }
    case Qt::Key_Enter: //анимация
        m_is_animate = !m_is_animate;
        break;
    }

    m_view.setToIdentity();
    //формируем вид камеры
    m_view.lookAt(m_position,
        m_center,
        QVector3D(0, 1, 0));
    updateGL(); //обновляем только на действие пользователя
}

void GLWidget::animate()
{
    if (!m_is_animate) {
        return;
    }
    if (m_animate_state >= m_animate_states) { //перезапуск анимации
        m_animate_states = random_range(100.0f, 1000.0f);
        m_animate_state = 0;

        m_end_wind = m_new_wind;
        m_new_wind = random_range(0.0, 360.0);
        m_step_wind = (m_new_wind - m_end_wind) / m_animate_states;

        m_end_force_wind = m_new_force_wind;
        m_new_force_wind = random_range(0.0, 30.0);
        m_step_force_wind = (m_new_force_wind - m_end_force_wind) / m_animate_states;
    } else {
        m_end_wind += m_step_wind;
        m_end_force_wind += m_step_force_wind;
        m_animate_state++;
    }
}

void GLWidget::prepare()
{
    qsrand(QTime::currentTime().elapsed());
    m_model_scene_vpi = std::make_unique<Model>(PATH_SRC_SCENE);
    m_model_tree.push_back(std::make_unique<Model>(PATH_SRC_TREE_1));
    m_model_tree.push_back(std::make_unique<Model>(PATH_SRC_TREE_2));
    m_model_tree.push_back(std::make_unique<Model>(PATH_SRC_TREE_3));
    QList<QString> keys = m_model_scene_vpi->model()->store_shape.keys();
    //поиск фиктивных объектов - (расположение деревьев ) сцены
    for (int i = 0; i < keys.length(); ++i) {
        if (keys[i].startsWith("Point_tree")) {
            PrepareShape* shape = m_model_scene_vpi->model()->store_shape[keys[i]];
            //qDebug() << keys[i] << ": len = " << shape->position.length() ;
            if (shape->position.length() == 1) {
                m_position_tree.push_back(QPair<int, QVector3D>(
                    qrand() % m_model_tree.size(),
                    shape->position[0]));
                qDebug() << shape->position[0];
            }
        }
    }
}

void GLWidget::initializeGL()
{
    glClearColor(0.01f, 0.05f, 0.1f, 1.0f);
    // рассчет освещения
    glEnable(GL_LIGHTING);
    // двухсторонний расчет освещения
    glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
    glEnable(GL_NORMALIZE);
    m_aspectRatio = width() / height();
    //настройка камеры проекция + положение, взгляд
    m_projection.perspective(60, m_aspectRatio, 10, 1000);
    m_view.lookAt(m_position,
        m_center,
        QVector3D(0, 1, 0));
}

void GLWidget::paintGL()
{
    animate();
    m_angle_sun += 0.01;
    if (m_angle_sun >= 360.0) {
        m_angle_sun = 0.0;
    }
    float color_power = sin(m_angle_sun);
    //    QVector3D sun_power = color_power < 0.0 ?
    //                QVector3D(0.0, 0.0, 0.0):
    //                QVector3D(2.0, 2.0, 2.0) * color_power ;
    QVector3D sun_power = QVector3D(0.5, 0.5, 0.5);
    glClearColor(color_power, color_power, color_power, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    // устанавливаем параметры для того чтоб не было резких границ между прозрачным местом и текстурой:
    glAlphaFunc(GL_GREATER, 0.0);

    glEnable(GL_BLEND); //Включаем режим смешивания цветов
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //параметры смешивания

    GLfloat fogColor[] = { 0.3f, 0.3f, 0.3f };
    glEnable(GL_FOG); // Включает туман (GL_FOG)
    glFogi(GL_FOG_MODE, GL_LINEAR); // Выбираем тип тумана
    glFogfv(GL_FOG_COLOR, fogColor); // Устанавливаем цвет тумана
    glFogf(GL_FOG_DENSITY, 0.4f); // Насколько густым будет туман
    glHint(GL_FOG_HINT, GL_DONT_CARE); // Вспомогательная установка тумана
    glFogf(GL_FOG_START, 1.0f); // Глубина, с которой начинается туман
    glFogf(GL_FOG_END, 100.0f); // Глубина, где туман заканчивается.

    glEnable(GL_LIGHT0);
    GLfloat light0_diffuse[3];
    light0_diffuse[0] = color_power * 5;
    light0_diffuse[1] = color_power * 5;
    light0_diffuse[2] = color_power * 5;

    GLfloat light0_position[4];
    light0_position[0] = 0.0;
    light0_position[1] = 100.0f * sin(m_angle_sun);
    light0_position[2] = 100.0f * cos(m_angle_sun);
    light0_position[3] = 1.0;

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_position);

    QMatrix4x4 mainModelView, trasform_wind;
    mainModelView.setToIdentity();
    QMatrix4x4 vp;
    vp = m_projection * m_view * m_viewTransform;
    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(vp.data());
    glMatrixMode(GL_MODELVIEW);
    //mainModelView.scale(4.0);//визуально увеличиваем модель
    mainModelView = m_modelTransform * mainModelView;
    m_model_scene_vpi->draw(mainModelView);

    //    QVector3D asix_y = QVector3D(0.0, 1.0, 0.0) + QVector3D(1.0, 0.0, 1.0);//*(qrand()%10)/10.0;//m_wind;

    for (int i = 0; i < m_position_tree.size(); ++i) {
        //        trasform_wind = QMatrix4x4(
        //                    1.0, 0.0, 0.0, 0.0,
        //                    asix_y.x(), asix_y.y(), asix_y.z(), 0.0,
        //                    0.0, 0.0, 1.0, 0.0,
        //                    0.0, 0.0, 0.0, 1.0
        //                    );
        trasform_wind.setToIdentity();
        //trasform_wind.rotate(m_end_wind, QVector3D(0.0, 1.0, 0.0));//направление ветра
        trasform_wind.rotate(m_end_force_wind,
            QVector3D(
                     sin(m_end_wind),
                     0.0,
                     cos(m_end_force_wind))); //сила ветра, наклон

        mainModelView.setToIdentity();
        mainModelView.translate(m_position_tree[i].second);
        mainModelView.scale(4.0);
        //mainModelView.rotate(m_force_wind, m_wind);
        mainModelView = m_modelTransform * mainModelView * trasform_wind;
        m_model_tree[m_position_tree[i].first]->draw(mainModelView);
    }
    swapBuffers();
}

GLWidget::~GLWidget()
{
}
