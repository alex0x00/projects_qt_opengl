#include <QApplication>
#include "glwidget.h"

int main(int argc, char** argv)
{
    QApplication a(argc, argv);
    GLWidget glwidget;
    glwidget.setGeometry(0,0, 1024, 768);
    glwidget.show();
    return a.exec();
}
