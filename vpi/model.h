#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QtOpenGL>
#include "objloader.h"

typedef QMap<QString, GLuint> StoreTextures;

class Model {
public:
    enum ModeDraw {
        Points = GL_POINTS,
        Triangles = GL_TRIANGLES,
        Lines = GL_LINES
    };
    Model();
    Model(const QString& filename);
    void load(const QString& filename);
    void draw(const QMatrix4x4& modelview);
    void draw();
    ModeDraw modeDraw() const;
    void setModeDraw(const ModeDraw& modeDraw);
    QMatrix4x4 modelTransform() const;
    void setModelTransform(const QMatrix4x4& modelTransform);
    bool useMaterial(const QString& name);

    QMap<QString, GLuint>& storeTextures()
    {
        return m_store_textures;
    }
    const  DeserializationObj* model()
    {
        return m_model;
    }

private:
    void useMaterial(Material* material);
    ModeDraw m_modeDraw;
    DeserializationObj* m_model;
    QMatrix4x4 m_modelTransform;
    StoreTextures m_store_textures;
};

#endif // MODEL_H
