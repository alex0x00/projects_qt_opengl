#include "../include/model.h"

Model::Model()
{
    m_modeDraw = Triangles;
    m_program = new QGLShaderProgram;
}

Model::Model(const QString& filename)
{
    m_modeDraw = Triangles;
    m_program = new QGLShaderProgram;
    load(filename);
}
void Model::load(const QString& filename)
{
    m_model = new DeserializationObj;
    m_model->load(filename);
    //определение файлов текстур для последующей загрузки
    foreach(DeserializationMtl * fileMtl, m_model->store_Mtl)
    {
        foreach(Material * material, fileMtl->store_material)
        {
            if (!material->diffuse_texture_name.isEmpty()) {
                m_store_textures.insert(material->diffuse_texture_name, 0);
            }
        }
    }
    qDebug() << "loading model";
}
bool Model::setFragmentShaderFile(const QString& filename)
{
    if (!m_program->addShaderFromSourceFile(QGLShader::Fragment, filename)) {
        qDebug() << "Could not load shader file " + filename + ": " << m_program->log();
        return false;
    } else {
        qDebug() << "Loaded " + filename + " successfully";
        return true;
    }
}
bool Model::setVertexShaderFile(const QString& filename)
{
    if (!m_program->addShaderFromSourceFile(QGLShader::Vertex, filename)) {
        qDebug() << "Could not load shader file " + filename + ": " << m_program->log();
        return false;
    } else {
        qDebug() << "Loaded " + filename + " successfully";
        return true;
    }
}
bool Model::linkShaderProgram()
{
    if (m_program->link()) {
        qDebug() << "Program linked";
        return true;
    } else {
        qDebug() << "Failed to link program:" << m_program->log();
        return false;
    }
}

void Model::initShaderProgram()
{
    //находим индексы аттрибутов
    a_position = m_program->attributeLocation("a_position");
    a_normal = m_program->attributeLocation("a_normal");
    a_texcoord = m_program->attributeLocation("a_texcoord");
    u_transform_model = m_program->uniformLocation("u_transform_model");
    u_transform_normal = m_program->uniformLocation("u_transform_normal");
}
Model::ModeDraw Model::modeDraw() const
{
    return m_modeDraw;
}

void Model::setModeDraw(const ModeDraw& modeDraw)
{
    m_modeDraw = modeDraw;
}

QMatrix4x4 Model::modelTransform() const
{
    return m_modelTransform;
}

void Model::setModelTransform(const QMatrix4x4& modelTransform)
{
    m_modelTransform = modelTransform;
}

void Model::useMaterial(const QString& name)
{
    auto material = m_model->material(name);
    if (material) {
        useMaterial(material);
    } else {
        qDebug() << "not find material:" << name;
    }
}

QMap<QString, GLuint> &Model::storeTextures()
{
    return m_store_textures;
}

const DeserializationObj * Model::model()
{
    return m_model;
}

void Model::useMaterial(Material* material)
{
    m_program->bind();
    QVector4D ambient, diffuse, specular, emission;
    float shininess;
    float dissolve = material->dissolve;

    ambient = material->ambient;
    diffuse = material->diffuse;
    specular = material->specular;
    emission = material->emission;
    shininess = material->shininess;
    //TODO прозрачность можно сделать в шейдере 
    if (dissolve < 1.0f) {
        ambient.setW(dissolve);
        diffuse.setW(dissolve);
        specular.setW(dissolve);
        emission.setW(dissolve);
    }
    m_program->setUniformValue("u_material_ambient", ambient);
    m_program->setUniformValue("u_material_diffuse", diffuse);
    m_program->setUniformValue("u_material_specular", specular);
    m_program->setUniformValue("u_material_emission", emission);
    m_program->setUniformValue("u_material_shininess", shininess);
    if (m_store_textures.contains(material->diffuse_texture_name)) {
        GLuint texture = m_store_textures.value(material->diffuse_texture_name);
        //FIXME не находит ссылки на функции OpenGL
//        glBindTexture(GL_TEXTURE_2D, texture);
//        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //glActiveTexture(GL_TEXTURE0);
        //используем 1 активную текстуру
        m_program->setUniformValue("u_material_texture", texture); //texture = 0
    }
}
QGLShaderProgram* Model::program() const
{
    return m_program;
}

void Model::setProgram(QGLShaderProgram* program)
{
    m_program = program;
    initShaderProgram();
}

void Model::draw(const QMatrix4x4& world)
{
    m_program->bind();
    //переводим в мировые координаты
    m_program->setUniformValue(u_transform_model, world);
    QMatrix4x4 normal = world;
    normal.normalMatrix();
    m_program->setUniformValue(u_transform_normal, normal);
    foreach(PrepareShape * shape, m_model->store_shape)
    {
        //устанавливаем материал
        useMaterial(shape->material);
        m_program->enableAttributeArray(a_position);
        m_program->setAttributeArray(a_position, shape->position.constData());

        if (shape->isUseNormal) {
            m_program->enableAttributeArray(a_normal);
            m_program->setAttributeArray(a_normal, shape->normal.constData());
        }
        if (shape->isUseTexcord) {
            m_program->enableAttributeArray(a_texcoord);
            m_program->setAttributeArray(a_texcoord, shape->texcoord.constData());
        }
        glDrawArrays(m_modeDraw, 0, shape->position.size());
        m_program->disableAttributeArray(a_position);
        m_program->disableAttributeArray(a_normal);
        m_program->disableAttributeArray(a_texcoord);
        useMaterial(shape->material);
    }
    m_program->release();
    //m_program.bind();
}

void Model::draw()
{
    draw(m_modelTransform);
}
