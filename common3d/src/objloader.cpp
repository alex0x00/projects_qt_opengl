#include "../include/objloader.h"

PrepareShape::PrepareShape(const QString& name_shape)
    : name(name_shape)
{
    isUseNormal = isUsePosition = isUseTexcord = true;
}

PrepareShape::PrepareShape()
    : name("default") //по умалчанию
{
    isUseNormal = isUsePosition = isUseTexcord = true;
}

void PrepareShape::addPosition(QVector3D* pos)
{
    if (isUsePosition) {
        position.push_back(*pos);
    } else {
        qCritical() << "Position is not use!";
    }
}

void PrepareShape::addNormal(QVector3D* norm)
{
    if (isUseNormal) {
        normal.push_back(*norm);
    } else {
        qCritical() << "Normal is not use!";
    }
}

void PrepareShape::addTexcoord(QVector2D* tex)
{
    if (isUseTexcord) {
        texcoord.push_back(*tex);
    } else {
        qCritical() << "Texture coordinate is not use!";
    }
}

void VertexArrayData::set(PrepareShape* shape)
{
    offsetPosition = offsetNormal = 0;
    offsetTextureCoordinate = size = unit_size = 0;

    isUseNormal = shape->isUseNormal;
    isUsePosition = shape->isUsePosition;
    isUseTexcord = shape->isUseTexcord;

    //проверяем корректность данных

    if (isUsePosition) {
        size = shape->position.length();
        //qDebug() << "size" << shape->position.size() << "length" << shape->position.length();
        unit_size += 3; // QVector3D
    } else {
        qCritical() << "Error is not positions attribute";
        return;
    }

    if (isUseNormal) {
        if (shape->position.length() == shape->normal.length()) {
            offsetNormal = unit_size;
            unit_size += 3; // QVector3D
        } else {
            qCritical() << "Error is not correct length normal attribute";
            return;
        }
    }

    if (isUseTexcord) {
        if (shape->position.length() == shape->texcoord.length()) {
            offsetTextureCoordinate = unit_size;
            unit_size += 2; // QVector2D
        } else {
            qCritical() << "Error is not correct length "
                           "texture coordinate attribute";
            return;
        }
    }
    arrayData = new float[unit_size * size];
    //TODO объеденить циклы и наложить векторы на память, без чтения полей
    for (unsigned int i = 0; i < size; ++i) {
        arrayData[unit_size * i + 0] = shape->position[i].x();
        arrayData[unit_size * i + 1] = shape->position[i].y();
        arrayData[unit_size * i + 2] = shape->position[i].z();
    }
    if (shape->isUseNormal) {
        for (unsigned int i = 0; i < size; ++i) {
            arrayData[offsetNormal + unit_size * i + 0] = shape->normal[i].x();
            arrayData[offsetNormal + unit_size * i + 1] = shape->normal[i].y();
            arrayData[offsetNormal + unit_size * i + 2] = shape->normal[i].z();
        }
    }
    if (shape->isUseTexcord) {
        for (unsigned int i = 0; i < size; ++i) {
            arrayData[offsetTextureCoordinate + unit_size * i + 0] = shape->texcoord[i].x();
            arrayData[offsetTextureCoordinate + unit_size * i + 1] = shape->texcoord[i].y();
        }
    }
}

VertexArrayData::~VertexArrayData()
{
    if (size != 0) {
        delete[] arrayData;
    }
}

DeserializationMtl::~DeserializationMtl()
{
    store_is_not_parse.clear();

    QMapIterator<QString, Material*> i(store_material);
    // QMap<QString, Material*> store;
    while (i.hasNext()) {
        i.next();
        delete i.value();
    }
}

DeserializationObj::~DeserializationObj()
{
    using_material.clear();
    store_is_not_parse.clear();

    QMapIterator<QString, DeserializationMtl*> i1(store_Mtl);
    // QMap<QString, Material*> store;
    while (i1.hasNext()) {
        i1.next();
        delete i1.value();
    }
    QMapIterator<QString, PrepareShape*> i2(store_shape);
    // QMap<QString, Material*> store;
    while (i2.hasNext()) {
        i2.next();
        delete i2.value();
    }
}

QVector<QString> parseTokens(const QString& str)
{
    QVector<QString> tokens;

    int begin = 0, end = 0, length = str.length();
    bool is_word = false;

    for (int i = 0; i < length; ++i) {
        if (str[i].isSpace()) //конец токена
        {
            if (is_word) {
                end = i;
                is_word = false;
                tokens.push_back(str.mid(begin, end - begin));
            }
        } else if (!is_word) {
            begin = i;
            is_word = true; //начало токена
        }
    }

    if (is_word) {
        end = length;
        tokens.push_back(str.mid(begin, end - begin));
    }
    return tokens;
}

VertexIndex parseTriple(const QString& str)
{
    VertexIndex ret;
    QStringList indices = str.split("/");
    int count = indices.size();
    switch (count) {
    //проваливаение, для чтения всех возможных атрибутов
    case 3:
        ret.normal = indices.at(2).toInt();
    case 2:
        ret.texcoord = indices.at(1).toInt();
    case 1:
        ret.position = indices.at(0).toInt();
        break;
    default:
        qDebug() << "Parese triple error >3 or 0 ? ";
    }
    //    if (count > 0) {
    //        ret.position = indices.at(0).toInt();
    //    }
    //    if (count > 1) {
    //        ret.texcoord = indices.at(1).toInt();
    //    }
    //    if (count > 2) {
    //        ret.normal = indices.at(2).toInt();
    //    }
    return ret;
}

QVector4D parseColor(const QString& str)
{
    QVector<QString> tokens = parseTokens(str);
    QVector4D ret;
    int count = tokens.size();

    if (count >= 4) //первый токен код операции остальные три float
    {
        float r, g, b, a = 1.0f;
        r = tokens[1].toFloat();
        g = tokens[2].toFloat();
        b = tokens[3].toFloat();
        ret = QVector4D(r, g, b, a);
        if (count >= 5) //если есть альфа канал
        {
            a = tokens[4].toFloat();
            ret.setW(a);
        }
    } else {
        qDebug() << "error parse color in line:" << str;
    }
    return ret;
}

float parseFloat(const QString& str)
{
    QVector<QString> tokens = parseTokens(str);
    float ret = 0.0;
    int count = tokens.size();

    if (count >= 2) //первый токен код операции
    {
        ret = tokens[1].toFloat();
    } else {
        qDebug() << "error parse color in line:" << str;
    }
    return ret;
}

void DeserializationMtl::addMaterial(Material* current)
{
    //добавляем  для этого подберем подходещие имя
    if (!store_material.contains(current->name)) {
        store_material.insert(current->name, current);
    } else {
        QString new_name(current->name);
        for (int i = 0;; ++i) {
            new_name += "." + QString::number(i);
            if (!store_material.contains(new_name)) {
                store_material.insert(new_name, current);
                break;
            }
        }
    }
}

void DeserializationMtl::load(const QString& src)
{
    QFile file(src);
    if (!file.open(QIODevice::ReadOnly)) {
        QFileInfo path_file(file);
        qWarning() << "File is not open." << path_file.absoluteFilePath();
        return;
    }

    QString line;
    // QVector<QString>* tokens;
    bool is_ready_material = false;
    Material* current = new Material;
    QVector<QString> tokens;

    while (!file.atEnd()) {
        line = file.readLine();
        if (line == "\n" || line == " " || line.isEmpty()) {
            continue;
        }
        tokens = parseTokens(line);

        if (tokens[0] == "#") {
            //пропускаем коменты
            // continue;
        } else if (tokens[0] == "newmtl") {
            //если первый обект без названия, а второй с названием
            if (is_ready_material) {
                //сохранеяем текущию форму и определяем новую форму
                addMaterial(current);
                current = new Material;
                is_ready_material = false;
            }
            current->name = tokens[1];
        } else if (tokens[0] == "Ka") {
            is_ready_material = true;
            current->ambient = parseColor(line);
        } else if (tokens[0] == "Kd") {
            is_ready_material = true;
            current->diffuse = parseColor(line);
        } else if (tokens[0] == "Ks") {
            is_ready_material = true;
            current->specular = parseColor(line);
        } else if (tokens[0] == "Tf") {
            is_ready_material = true;
            current->transmission_filter = parseColor(line);
        } else if (tokens[0] == "Ke") {
            is_ready_material = true;
            current->emission = parseColor(line);
        } else if (tokens[0] == "Ns") {
            is_ready_material = true;
            current->shininess = parseFloat(line);
        } else if (tokens[0] == "Ni") {
            is_ready_material = true;
            current->optical_density = parseFloat(line);
        } else if (tokens[0] == "d") {
            is_ready_material = true;
            current->dissolve = parseFloat(line);
        } else if (tokens[0] == "illum") {
            is_ready_material = true;
            current->shininess = tokens[1].toInt();
        } else if (tokens[0] == "map_Ka") {
            is_ready_material = true;
            current->ambient_texture_name = tokens[1];
        } else if (tokens[0] == "map_Kd") {
            is_ready_material = true;
            current->diffuse_texture_name = tokens[1];
        } else if (tokens[0] == "map_Ks") {
            is_ready_material = true;
            current->specular_texture_name = tokens[1];
        } else if (tokens[0] == "map_Ns") {
            is_ready_material = true;
            current->shininess_texture_name = tokens[1];
        } else if (tokens[0] == "map_d") {
            is_ready_material = true;
            current->dissolve_texture_name = tokens[1];
        } else if (tokens[0] == "map_n") {
            is_ready_material = true;
            current->normals_texture_name = tokens[1];
        } else {
            store_is_not_parse.push_back(line);
        }
    }
    if (is_ready_material) {
        addMaterial(current);
    }
}

void DeserializationObj::addShape(PrepareShape* current)
{
    //добавляем текущию форму, для этого подберем подходящие имя
    if (!store_shape.contains(current->name)) {
        store_shape.insert(current->name, current);
    } else {
        QString new_name(current->name);
        for (int i = 0;; ++i) {
            new_name += "." + QString::number(i);
            if (!store_shape.contains(new_name)) {
                store_shape.insert(new_name, current);
                break;
            }
        }
    }
}

int DeserializationObj::fixIndex(int index, int length)
{
    int i;
    if (index > 0) {
        i = index - 1;
    } else if (index == 0) {
        i = 0;
    } else {
        //относительный идекс
        i = length + index;
    }
    return i;
}

Material* DeserializationObj::material(const QString& name)
{
    QMapIterator<QString, DeserializationMtl*> i(store_Mtl);
    // QMap<QString, Material*> store;
    while (i.hasNext()) {
        i.next();
        // store = i.value()->store_material;
        QMapIterator<QString, Material*> j(i.value()->store_material);
        while (j.hasNext()) {
            j.next();
            if (j.value()->name == name) {
                return j.value();
            }
        }
    }
    return nullptr;
}

void DeserializationObj::load(const QString& src)
{
    QFile file(src);
    if (!file.open(QIODevice::ReadOnly)) {
        QFileInfo path_file(file);
        qWarning() << "File is not open for path: "
                   << path_file.absoluteFilePath();
        return;
    }

    QString line;
    QList<Shape*> shapes;
    VertexData array_vertices; //буфер всех вершин
    float x, y, z;
    x = y = z = 0.0;

    bool is_ready_shape = false; //если читали данные true

    Shape* current = new Shape;
    //удалена функциональность корекции данных
    //при определении следующего обекта v, vn, vt, f
    QVector<QString> tokens;

    while (!file.atEnd()) {
        line = file.readLine();
        if (line == "\n" || line == " " || line.isEmpty()) {
            continue;
        }
        tokens = parseTokens(line);

        if (tokens[0] == "#") {
            //пропускаем коменты
            // continue;
        } else if (tokens[0] == "mtllib") {
            QFileInfo path_file(file);
            QString src = path_file.absolutePath() + "/" + tokens[1];
            DeserializationMtl* file_Mtl = new DeserializationMtl;
            store_Mtl.insert(src, file_Mtl);
            file_Mtl->load(src);

        } else if (tokens[0] == "usemtl") {
            is_ready_shape = true;
            current->material = *material(tokens[1]);
            using_material.push_back(tokens[1]);
        } else if (tokens[0] == "o") {
            //если первый обект без названия, а второй с названием
            if (is_ready_shape) {
                //сохраняем текущию форму и определяем новую форму
                // addShape(current);
                shapes.push_back(current);
                current = new Shape;
                is_ready_shape = false;
            }
            current->name = tokens[1];
        } else if (tokens[0] == "v") {
            is_ready_shape = true;
            // Координаты X...
            x = tokens[1].toFloat();
            // Y...
            y = tokens[2].toFloat();
            // и Z.
            z = tokens[3].toFloat();
            // Добавляем vertex трехмерный вектор с координатами.
            QVector3D positions(x, y, z);
            array_vertices.positions.push_back(positions);
        } else if (tokens[0] == "vn") {
            is_ready_shape = true;
            // нормали.
            // Координаты X...
            x = tokens[1].toFloat();
            // Y...
            y = tokens[2].toFloat();
            // и Z.
            z = tokens[3].toFloat();
            QVector3D normal(x, y, z);
            array_vertices.normal.push_back(normal);
        } else if (tokens[0] == "vt") {
            is_ready_shape = true;
            // Координаты X...
            x = tokens[1].toFloat();
            // Y...
            y = tokens[2].toFloat();
            QVector2D texcoords(x, y);
            array_vertices.texcoords.push_back(texcoords);
        } else if (tokens[0] == "f") {
            is_ready_shape = true;
            //записываем индыксы как есть т.е не с 0 а с 1
            Face* face = new Face;
            face->points = tokens.size() - 1;
            for (unsigned int i = 0; i < face->points; ++i) {
                //загружаем если есть вершины
                face->indices.push_back(parseTriple(tokens[1 + i]));
            }
            // добавляем в список faces.
            current->faces.push_back(face);
        } else {
            store_is_not_parse.push_back(line);
        }
    }
    if (is_ready_shape) {
        // addShape(current);
        shapes.push_back(current);
    }
    //заполним вершинами
    VertexIndex vertex_index;
    //составим формы из загруженных данных

    QListIterator<Shape*> _shapes(shapes);
    PrepareShape* new_shape = 0;
    //можно сделать оптимально если создавать индексы вершин
    while (_shapes.hasNext()) { //по всем формам
        current = _shapes.next();
        new_shape = new PrepareShape;
        new_shape->name = current->name;
        new_shape->material = current->material.name;

        for (int i = 0; i < current->faces.length(); ++i) { //по всем граням
            for (int j = 0, v, vt, vn; j < current->faces[i]->indices.length();
                 ++j) { //по всем индексам вершин
                vertex_index = current->faces[i]->indices[j];
                //добавлем только то, что нужно
                if (vertex_index.position == 0) {
                    new_shape->isUsePosition = false;
                }
                if (new_shape->isUsePosition) {
                    v = fixIndex(vertex_index.position,
                        array_vertices.positions.length());
                    new_shape->addPosition(new QVector3D(array_vertices.positions[v]));
                }

                if (vertex_index.normal == 0) {
                    new_shape->isUseNormal = false;
                }
                if (new_shape->isUseNormal) {
                    vn = fixIndex(vertex_index.normal, array_vertices.normal.length());
                    new_shape->addNormal(new QVector3D(array_vertices.normal[vn]));
                }

                if (vertex_index.texcoord == 0) {
                    new_shape->isUseTexcord = false;
                }
                if (new_shape->isUseTexcord) {
                    vt = fixIndex(vertex_index.texcoord,
                        array_vertices.texcoords.length());
                    new_shape->addTexcoord(new QVector2D(array_vertices.texcoords[vt]));
                }
            }
        }
        delete current;
        addShape(new_shape);
    }
    shapes.clear();
}
