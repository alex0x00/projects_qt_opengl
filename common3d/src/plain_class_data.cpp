#include "../include/plain_class_data.h"

Material::Material()
    : name("default")
    , shininess{}
    , optical_density{}
    , dissolve{}
    , illumination{}
{
}

Material::Material(const QString& name_material)
    : name(name_material)
    , shininess{ 1.f }
    , optical_density{ 1.f }
    , dissolve{ 1.f }
    , illumination{}
{
}

Shape::Shape(const QString& name_shape)
    : name(name_shape)
{
}

Shape::~Shape()
{
    for (int i = 0; i < faces.length(); ++i) {
        delete faces[i];
    }
    faces.clear();
}

Shape::Shape()
    : name("default") //по умалчанию
{
}

void Vertex::setPosition(const QVector3D& pos)
{
    // isUsePosition= true;
    position = pos;
}

void Vertex::setNormal(const QVector3D& norm)
{
    // isUseNormal= true;
    normal = norm;
}

void Vertex::setTexcoord(const QVector2D& tex)
{
    // isUseTexcord= true;
    texcoord = tex;
}

bool operator==(const VertexIndex& arg1, const VertexIndex& arg2)
{
    return (arg1.position == arg2.position)
        && (arg1.normal == arg2.normal)
        && (arg1.texcoord == arg2.texcoord);
}
