#ifndef MODEL_H
#define MODEL_H

#include <QObject>
#include <QtOpenGL>
#include "objloader.h"

class Model {
public:
    using StoreTextures = QMap<QString, GLuint>;
    enum ModeDraw {
        Points      = GL_POINTS,
        Triangles   = GL_TRIANGLES,
        Lines       = GL_LINES
    };
    Model();
    Model(const QString& filename);
    void load(const QString& filename);
    void draw(const QMatrix4x4& modelview);
    void draw();
    bool setVertexShaderFile(const QString& filename);
    bool setFragmentShaderFile(const QString& filename);
    bool linkShaderProgram();
    void initShaderProgram();
    ModeDraw modeDraw() const;
    void setModeDraw(const ModeDraw& modeDraw);
    QMatrix4x4 modelTransform() const;
    void setModelTransform(const QMatrix4x4& modelTransform);
    void useMaterial(const QString& name);
    QMap<QString, GLuint>& storeTextures();
    const  DeserializationObj* model();
    QGLShaderProgram* program() const;
    void setProgram(QGLShaderProgram* program);
private:
    void useMaterial(Material* material);
    
    DeserializationObj* m_model;
    QGLShaderProgram* m_program;
	QMatrix4x4 m_modelTransform;
	ModeDraw m_modeDraw;
    GLuint a_position;
    GLuint a_normal;
    GLuint a_texcoord;
    GLuint u_transform_model;
    GLuint u_transform_normal;
    StoreTextures m_store_textures;
};

#endif // MODEL_H
