#ifndef OBJLOADER_H
#define OBJLOADER_H
#include "plain_class_data.h"
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QtCore>

// obj файл должен состоять из 3d вершин и нормалей и 2d текстурных координат
//на большенство возможных ошибок поставлены информаторы см консоль или файл
//вывода
// неточности в формате описания модели выводятся в отладке (консоль и тп)

struct PrepareShape { //Подготовка и распаковка данных
    QString name;
    QString material;
    // QVector<Vertex*> array_vertices;
    QVector<QVector3D> position;
    QVector<QVector3D> normal;
    // QColor color; // или QVector3D color;
    QVector<QVector2D> texcoord;
    // QVector<unsigned int> indices;

    PrepareShape(const QString& name_shape);
    PrepareShape();
    void addPosition(QVector3D* pos);
    void addNormal(QVector3D* norm);
    void addTexcoord(QVector2D* tex);
    bool isUseNormal;
    bool isUsePosition;
    bool isUseTexcord;
};

struct VertexArrayData {
    float* arrayData;
    //последовательность данных: позиция, нормаль, текстурные координаты
    //если они есть
    unsigned int offsetPosition;
    unsigned int offsetNormal;
    unsigned int offsetTextureCoordinate;
    unsigned int size;
    unsigned int unit_size;
    bool isUseNormal;
    bool isUsePosition;
    bool isUseTexcord;

    void set(PrepareShape* shape);
    ~VertexArrayData();
};
struct DeserializationMtl {
    //название и материал
    QMap<QString, Material*> store_material;
    //токен и остальное после пустого пространства
    QStringList store_is_not_parse; //хранилище не распознаного

    void addMaterial(Material* current);
    void load(const QString& src);
    ~DeserializationMtl();
};

struct DeserializationObj {
    //путь и распознаный файл материала
    QMap<QString, DeserializationMtl*> store_Mtl;
    // QMap<QString, Material*> using_material;
    QStringList using_material; //список исп материалов
    QMap<QString, PrepareShape*> store_shape;
    QStringList store_is_not_parse;

    Material* material(const QString& name);
    void load(const QString& src);
    void addShape(PrepareShape* current);
    ~DeserializationObj();

private:
    static int fixIndex(int index, int length);
};

#endif
