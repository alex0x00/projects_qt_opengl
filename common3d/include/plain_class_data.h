#ifndef PLAIN_CLASS_DATA_H
#define PLAIN_CLASS_DATA_H
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>
#include <QtCore>
//****************************
// Пример описания материала
// цвет в формате rgb (color r g b) r g b float range [0.0, 1.0]
// scalar float
//# Объявление материала c название name
// newmtl name
//# Цвета
// Ka color # Цвет окружающего освещения
// Kd color # Диффузный цвет
// Ke color # Cвечение материала

//#Параметры отражения
// Ns scalar # Коэффициент зеркального отражения int range [0, 1000]
// Ks color # Цвет зеркального отражения
// Tf color фильтр передачи

// Ni scalar # Показатель преломления или оптическая плотность
// d scalar # Прозрачность не зависит от модели освещения 0 прозрачный 1 не
// прозрачный
//#(Материал распустить или растворить умножая на значение текстуры)

// illum 2 # Задает модель освещения в материале int range [0, 10]
// Подробнее:
// 0	 Color on and Ambient off
// 1	 Color on and Ambient on
// 2	 Highlight on
// 3	 Reflection on and Ray trace on
// 4	 Transparency: Glass on
// Reflection: Ray trace on
// 5	 Reflection: Fresnel on and Ray trace on
// 6	 Transparency: Refraction on
// Reflection: Fresnel off and Ray trace on
// 7	 Transparency: Refraction on
// Reflection: Fresnel on and Ray trace on
// 8	 Reflection on and Ray trace off
// 9	 Transparency: Glass on
// Reflection: Ray trace off
// 10	 Casts shadows onto invisible surfaces
// http://www.fileformat.info/format/material/
// Ссылки на текстуры
// map_Ka name
// map_Kd name
// map_Ks name
// map_Ns name
// map_d name
//*****************************
struct Material {
    QString name;
    QVector4D ambient; // Ka
    QVector4D diffuse; // Kd
    QVector4D specular; // Ks
    QVector4D transmission_filter; // Tf
    QVector4D emission; // Ke
    float shininess; // Ns
    float optical_density; // Ni
    float dissolve; // d
    int illumination; // illum
    QString ambient_texture_name; // map_Ka
    QString diffuse_texture_name; // map_Kd
    QString specular_texture_name; // map_Ks
    QString shininess_texture_name; // map_Ns
    QString dissolve_texture_name; // map_d
    QString normals_texture_name; // map_n добавим текстуру нормалей
    Material();
    Material(const QString& name_material);
};

struct VertexData { //сырые данные (считанные данные из файла модели)
    QVector<QVector3D> positions;
    QVector<QVector3D> normal;
    QVector<QVector2D> texcoords;
    // QVector<QColor> colors;
    // QVector<unsigned int> indices;
};

struct Vertex { //вершинный атрибут базовые данные
    QVector3D position;
    QVector3D normal;
    QVector2D texcoord;
    // QColor color; // или QVector3D color;

    void setPosition(const QVector3D& pos);
    void setNormal(const QVector3D& norm);
    void setTexcoord(const QVector2D& tex);
};

//Форма объекта и его покрытие (материал)

struct VertexIndex { // 0 если не исп индекс
    int position{};
    int normal{};
    int texcoord{};
    //TODO оптимизация для индексов вершин
    friend bool operator==(const VertexIndex& arg1, const VertexIndex& arg2);
};
struct Face { //грани
    QVector<VertexIndex> indices;
    unsigned int points{ 3 }; //количество вершин на поверхность от 3 и более
};

struct Shape {
    QString name;
    Material material;
    QVector<Face*> faces;
    Shape();
    Shape(const QString& name_shape);
    ~Shape();
};
#endif // PLAIN_CLASS_DATA_H
