TARGET = common3d
TEMPLATE = lib
QT       += opengl
CONFIG += c++14
# staticlib
LIBS += -lopengl32
#INCLUDEPATH  += /include
SOURCES += src/*.cpp
HEADERS += include/*.h
