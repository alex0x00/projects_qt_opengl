#ifndef FLOWCHART_H
#define FLOWCHART_H

#include <QCoreApplication>
#include <QOpenGL.h>
#include <qopengl.h>
#include <QMatrix4x4>
#include <QVector3D>
#include <QVector2D>
#include <QVector>
#include <QImage>
#include <QString>
#include <QMap>

void checkError(int line = 0);

QVector<QString>& parseTokens(const QString& str);

struct Item {
    QVector<QVector3D> pos;
    QVector<QVector3D> norm;
};

class AlgoritmFlowchart {
public:
    AlgoritmFlowchart(const QString& src);
    AlgoritmFlowchart();

    virtual void draw()
    {
        qDebug() << "AlgoritmFlowchart.draw()";
    }

    QString path() const;
    void setPath(const QString& path);
    Item* item(const QString& name);
    QMatrix4x4 transform() const;
    void setTransform(const QMatrix4x4& transform);

protected:
    static QMap<QString, Item*>* m_storeItems; // = 0;
private:
    Item* loadObject3D(QString src);
    QString m_path;
    QMatrix4x4 m_transform;
};

class Vector : public AlgoritmFlowchart {
public:
    Vector(const QVector2D& pos, float angle);
    void draw();

private:
    QVector2D m_position;
    float m_angle;
};

class Line : public AlgoritmFlowchart {
public:
    Line(const QVector2D& pos, float angle, unsigned int length);
    void draw();

private:
    QVector2D m_position;
    unsigned int m_length;
    float m_angle;
};

class Block : public AlgoritmFlowchart {
public:
    Block(QString src, QString text);
    QMatrix4x4 textureTranslate() const;
    void setTextureTranslate(const QMatrix4x4& textureTranslate);

protected:
    void drawBlockText();
    void drawBlock();

private:
    void createTextureText(const QString& text);
    GLuint createTexture(const QImage& image,
                         GLenum target = GL_TEXTURE_2D,
                         GLint format = GL_RGBA);
    QVector<QString>& parseTokens(const QString& str);
    GLuint m_textureId;
    QMatrix4x4 m_textureTranslate;
};

class BlockBeginEnd : public Block {
public:
    BlockBeginEnd(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QVector2D m_position;
    QVector2D position() const;
    void setPosition(const QVector2D& position);
};

class BlockConditional : public Block {
public:
    BlockConditional(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QVector2D position() const;
    void setPosition(const QVector2D& position);

private:
    QVector2D m_position;
};
//action._obj
class BlockAction : public Block {
public:
    BlockAction(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QVector2D position() const;
    void setPosition(const QVector2D& position);

private:
    QVector2D m_position;
};

class BlockInputOutput : public Block {
public:
    BlockInputOutput(const QVector2D& pos, const QString& text);
    void draw()
    {
        drawBlock();
        drawBlockText();
    }
    QVector2D position() const;
    void setPosition(const QVector2D& position);

private:
    QVector2D m_position;
};
#endif // FLOWCHART_H
