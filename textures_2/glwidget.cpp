
#include <QtWidgets>
#include <QtOpenGL>

#include "glwidget.h"

GLWidget::GLWidget(QWidget* parent, QGLWidget* shareWidget)
    : QGLWidget(parent, shareWidget)
{
    clearColor = Qt::green;
    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(4);
    setFormat(fmt);
    pos = QVector3D(0, 0, 20);
    center = QVector3D(0, 0, 0);
}

GLWidget::~GLWidget()
{
}

void GLWidget::initializeGL()
{
    m_viewTransform.setToIdentity();
    aspectRatio = width() / height();
    m_projection.perspective(60, aspectRatio, 1, 1000);

    m_view.lookAt(QVector3D(0, 0, 20),
                  QVector3D(0, 0, 0),
                  QVector3D(0, 1, 0));

    glEnable(GL_DEPTH_TEST);
}

void GLWidget::paintGL()
{
    glClearColor(0.3, 0.3, 0.3, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);

    QMatrix4x4 vp, block;

    vp = m_projection * m_view * m_viewTransform;

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(vp.data());
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    for (int i = 0; i < m_blocks.length(); ++i) {
        m_blocks[i]->draw();
    }
}

void GLWidget::resizeGL(int width, int height)
{
    glViewport(0, 0, width, height);
    aspectRatio = width / height;
    m_projection.setToIdentity();
    m_projection.perspective(60, aspectRatio, 1, 1000);
    updateGL();
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    float step = 1.0f;
    //выходи по escape
    if (event->key() == Qt::Key_Escape) {
        exit(0);
    } else if (event->key() == Qt::Key_8) {
        pos += QVector3D(0.0, step, 0.0);
        center += QVector3D(0.0, step, 0.0);
    } else if (event->key() == Qt::Key_2) {
        pos += QVector3D(0.0, -step, 0.0);
        center += QVector3D(0.0, -step, 0.0);
    } else if (event->key() == Qt::Key_4) {
        pos += QVector3D(step, 0.0, 0.0);
        center += QVector3D(step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_6) {
        pos += QVector3D(-step, 0.0, 0.0);
        center += QVector3D(-step, 0.0, 0.0);
    } else if (event->key() == Qt::Key_Up) {
        m_viewTransform.rotate(step, QVector3D(1, 0, 0));
    } else if (event->key() == Qt::Key_Down) {
        m_viewTransform.rotate(-step, QVector3D(1, 0, 0));
    } else if (event->key() == Qt::Key_Left) {
        m_viewTransform.rotate(step, QVector3D(0, 1, 0));
    } else if (event->key() == Qt::Key_Right) {
        m_viewTransform.rotate(-step, QVector3D(0, 1, 0));
    }
    m_view.setToIdentity();
    m_view.lookAt(pos,
                  center,
                  QVector3D(0, 1, 0));
    updateGL();
}

void GLWidget::algoritmBlocksLoad(QString src)
{
    QString path_src = src;
    if (!QFile::exists(path_src)) {
        path_src = QCoreApplication::applicationDirPath() + "/" + src;
        if (!QFile::exists(path_src)) {
            qWarning() << "Algoritm file not found:" << path_src;
            return;
        }
    }
    enum AlgoritmBloksFileFormat {
        NONE = 0,
        BLOCK_BEGIN_END,
        BLOCK_ACTION,
        BLOCK_CONDITIONAL,
        BLOCK_INPUT_OUTPUT,
        VECTOR,
        LINE
    } state = NONE;
    QFile file(path_src);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "Algoritm file not opened:";
    }
    QString line;
    QVector<QString> tokens;
    AlgoritmFlowchart* element = 0;
    QString text;
    QVector2D pos;
    unsigned int length;
    float angle;

    while (!file.atEnd()) {
        line = file.readLine();
        tokens = parseTokens(line);
        if (tokens.length() == 0) {
            continue;
        }
        if (tokens[0] == "begin") {

            if (tokens[1] == "BeginEnd") {
                state = BLOCK_BEGIN_END;
            } else if (tokens[1] == "Conditional") {
                state = BLOCK_CONDITIONAL;
            } else if (tokens[1] == "Action") {
                state = BLOCK_ACTION;
            } else if (tokens[1] == "InputOutput") {
                state = BLOCK_INPUT_OUTPUT;
            } else if (tokens[1] == "Vector") {
                state = VECTOR;
            } else if (tokens[1] == "Line") {
                state = LINE;
            }
        } else if (tokens[0] == "pos") {
            float x, y;
            x = tokens[1].toFloat();
            y = tokens[2].toFloat();
            pos.setX(x);
            pos.setY(y);
        } else if (tokens[0] == "text") {
            text = line.remove(0, 5);
        } else if (tokens[0] == "angle") {
            angle = tokens[1].toFloat();
        } else if (tokens[0] == "length") {
            length = tokens[1].toUInt();
        } else if (tokens[0] == "end") {
            element = 0;
            switch (state) {
            case NONE:
                break;
            case BLOCK_BEGIN_END:
                element = new BlockBeginEnd(pos, text);
                break;
            case BLOCK_CONDITIONAL:
                element = new BlockConditional(pos, text);
                break;
            case BLOCK_ACTION:
                element = new BlockAction(pos, text);
                break;
            case BLOCK_INPUT_OUTPUT:
                element = new BlockInputOutput(pos, text);
                break;
            case VECTOR:
                element = new Vector(pos, angle);
                break;
            case LINE:
                element = new Line(pos, angle, length);
                break;
            }
            if (element != 0) {
                m_blocks.push_back(element);
            }
        }
    }
    qDebug() << "Load file algoritm";
}
