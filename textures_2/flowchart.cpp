#include "flowchart.h"
#include <QPainter>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QGLWidget>

QMap<QString, Item*>* AlgoritmFlowchart::m_storeItems = 0;

Block::Block(QString src, QString text)
    : AlgoritmFlowchart(src)
{
    Item* currentItem = item(path());
    float eps = 1.0E-3;
    for (int i = 0; i < currentItem->pos.length(); ++i) {
        if (currentItem->pos[i].z() > 0)
            currentItem->pos[i].setZ(0.5 - eps);
        else
            currentItem->pos[i].setZ(-0.5 + eps);
    }
    createTextureText(text);
}

void Block::drawBlockText()
{
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_textureId);

    glPushMatrix();
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glRotatef(-90, 0.0, 0.0, 1.0);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(transform().data());

    glBegin(GL_QUADS);
    glVertex3f(-1.0, -1.0, 0.5);
    glTexCoord2f(0.0, 0.0);

    glVertex3f(-1.0, 1.0, 0.5);
    glTexCoord2f(0.0, 1.0);

    glVertex3f(1.0, 1.0, 0.5);
    glTexCoord2f(1.0, 1.0);

    glVertex3f(1.0, -1.0, 0.5);
    glTexCoord2f(1.0, 0.0);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

void Block::drawBlock()
{
    //qDebug() << m_item->pos.length();
    Item* currentItem = item(path());
    glColor3f(1.0, 1.0, 1.0);
    //checkError(__LINE__);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(transform().data());
    glBegin(GL_TRIANGLES);
    for (int i = 0; i < currentItem->pos.length(); ++i) {
        //glColor3f(1.0, 1.0, 1.0);
        glVertex3f(
            currentItem->pos[i].x(),
            currentItem->pos[i].y(),
            currentItem->pos[i].z());
        //        glNormal3f(
        //            currentItem->norm[i].x(),
        //            currentItem->norm[i].y(),
        //            currentItem->norm[i].z());
    }
    glEnd();
}

AlgoritmFlowchart::AlgoritmFlowchart(const QString& src)
{
    m_path = src;
    if (m_storeItems == 0)
        m_storeItems = new QMap<QString, Item*>;
    if (!m_storeItems->contains(m_path)) {
        Item* item = loadObject3D(m_path);
        m_storeItems->insert(m_path, item);
    }
}

AlgoritmFlowchart::AlgoritmFlowchart()
{
    m_path = QString("<none>");
    if (m_storeItems == 0)
        m_storeItems = new QMap<QString, Item*>;
}

QString AlgoritmFlowchart::path() const
{
    return m_path;
}

void AlgoritmFlowchart::setPath(const QString& path)
{
    m_path = path;
}

Item* AlgoritmFlowchart::item(const QString& name)
{
    if (m_storeItems->contains(m_path)) {
        return m_storeItems->value(name);
    } else {
        return 0;
    }
}

QMatrix4x4 AlgoritmFlowchart::transform() const
{
    return m_transform;
}

void AlgoritmFlowchart::setTransform(const QMatrix4x4& transform)
{
    m_transform = transform;
}

Item* AlgoritmFlowchart::loadObject3D(QString src)
{
    enum ReadState {
        NONE = 0,
        POSITION,
        NORMAL
    } state;

    if (!QFile::exists(src)) {
        QString path_src = QCoreApplication::applicationDirPath() + "/" + src;
        if (!QFile::exists(path_src)) {
            qWarning() << "File not found:" << path_src;
            return 0;
        } else {
            src = path_src;
        }
    }
    QFile file(src);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "File not opened:";
    }
    QString line;
    QVector<QString> tokens;
    Item* item = new Item;

    state = NONE;
    while (!file.atEnd()) {
        line = file.readLine();
        tokens = parseTokens(line);
        if (tokens[0] == "position") {
            state = POSITION;
        } else if (tokens[0] == "normal") {
            state = NORMAL;
        } else if (state != NONE) {
            QTextStream in(line.toUtf8());
            float x, y, z;
            in >> x >> y >> z;
            if (state == POSITION) {
                item->pos.push_back(QVector3D(x, y, z));
            } else if (state == NORMAL) {
                item->norm.push_back(QVector3D(x, y, z));
            }
        }
    }
    qDebug() << "Load model";
    return item;
}

void Block::createTextureText(const QString& text)
{
    QImage* image = new QImage(256, 256, QImage::Format_ARGB32);
    //QPixmap pix(QString(":/images/side%1.png").arg(1));
    QPainter painter;
    QFont font;
    font.setPixelSize(34);
    painter.begin(image);
    painter.setBrush(QBrush(QColor(Qt::white)));
    painter.drawRect(QRect(0, 0, 255, 255));
    painter.setBrush(QBrush(QColor(Qt::green)));
    painter.setFont(font);
    //painter.setBrush(QBrush(QColor(Qt::green)));
    painter.setPen(QPen(QColor(Qt::black)));
    painter.drawText(QRect(0, 0, 255, 255), Qt::AlignCenter, text);
    painter.end();
    m_textureId = createTexture(*image);
}

GLuint Block::createTexture(const QImage& image,
                            GLenum target,
                            GLint format)
{
    QImage prepare;
    prepare = QGLWidget::convertToGLFormat(image);
    ;
    GLuint texture = 0;
    //    C Specification
    //    void glTexImage2D(	GLenum target,
    //        GLint level,
    //        GLint internalFormat,
    //        GLsizei width,
    //        GLsizei height,
    //        GLint border,
    //        GLenum format,
    //        GLenum type,
    //        const GLvoid * data);

    glGenTextures(1, &texture);
    //checkError(__LINE__);

    glBindTexture(target, texture);
    //checkError(__LINE__);
    glTexImage2D(target,
                 0,
                 format,
                 prepare.width(),
                 prepare.height(),
                 0,
                 format,
                 GL_UNSIGNED_BYTE,
                 prepare.bits());
    //checkError(__LINE__);
    qDebug() << "texture" << texture;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    return texture;
}

QVector<QString>& parseTokens(const QString& str)
{
    QVector<QString>* tokens = new QVector<QString>;

    int begin = 0, end = 0, length = str.length();
    bool is_word = false;

    for (int i = 0; i < length; ++i) {
        if (str[i].isSpace()) //конец токена
        {
            if (is_word) {
                end = i;
                is_word = false;
                tokens->push_back(str.mid(begin, end - begin));
            }
        } else {
            if (!is_word) {
                begin = i;
                is_word = true; //начало токена
            }
        }
    }

    if (is_word) {
        end = length;
        tokens->push_back(str.mid(begin, end - begin));
    }
    return *tokens;
}
QMatrix4x4 Block::textureTranslate() const
{
    return m_textureTranslate;
}

void Block::setTextureTranslate(const QMatrix4x4& textureTranslate)
{
    m_textureTranslate = textureTranslate;
}

void checkError(int line)
{
    GLuint isError = glGetError();
    if (isError != GL_NO_ERROR) {
        qDebug() << "Error OpenGL code:"
                 << QString::number(isError, 16)
                 << endl
                 << "Line:"
                 << line;
    }
}

BlockBeginEnd::BlockBeginEnd(const QVector2D& pos, const QString& text)
    : Block(":/boxs/begin_end._obj", text)
{
    m_position = pos;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(m_position.x(),
                     m_position.y());
    setTransform(matrix);
}

QVector2D BlockBeginEnd::position() const
{
    return m_position;
}

void BlockBeginEnd::setPosition(const QVector2D& position)
{
    m_position = position;
}

BlockConditional::BlockConditional(const QVector2D& pos, const QString& text)
    : Block(QString(":/boxs/condition._obj"), text)
{
    m_position = pos;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(m_position.x(),
                     m_position.y());
    setTransform(matrix);
}

QVector2D BlockConditional::position() const
{
    return m_position;
}

void BlockConditional::setPosition(const QVector2D& position)
{
    m_position = position;
}

BlockAction::BlockAction(const QVector2D& pos, const QString& text)
    : Block(QString(":/boxs/action._obj"), text)
{
    m_position = pos;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(m_position.x(),
                     m_position.y());
    setTransform(matrix);
}

QVector2D BlockAction::position() const
{
    return m_position;
}

void BlockAction::setPosition(const QVector2D& position)
{
    m_position = position;
}

BlockInputOutput::BlockInputOutput(const QVector2D& pos, const QString& text)
    : Block(QString(":/boxs/input_output._obj"), text)
{
    m_position = pos;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(m_position.x(),
                     m_position.y());
    setTransform(matrix);
}

QVector2D BlockInputOutput::position() const
{
    return m_position;
}

void BlockInputOutput::setPosition(const QVector2D& position)
{
    m_position = position;
}

Vector::Vector(const QVector2D& pos, float angle)
    : AlgoritmFlowchart(QString(":/boxs/vector._obj"))
{
    m_position = pos;
    m_angle = angle;
    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(m_position.x(),
                     m_position.y());
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}

void Vector::draw()
{
    Item* currentItem = item(path());
    glColor3f(1.0, 1.0, 1.0);
    //checkError(__LINE__);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(transform().data());
    glBegin(GL_TRIANGLES);
    for (int i = 0; i < currentItem->pos.length(); ++i) {
        //glColor3f(1.0, 1.0, 1.0);
        glVertex3f(
            currentItem->pos[i].x(),
            currentItem->pos[i].y(),
            currentItem->pos[i].z());
        //            glNormal3f(
        //                currentItem->norm[i].x(),
        //                currentItem->norm[i].y(),
        //                currentItem->norm[i].z());
    }
    glEnd();
}

Line::Line(const QVector2D& pos, float angle, unsigned int length)
{
    m_position = pos;
    m_angle = angle;
    m_length = length;

    QMatrix4x4 matrix;
    matrix.setToIdentity();
    matrix.translate(m_position.x(),
                     m_position.y());
    //matrix.scale(m_length, 1.0, 1.0);
    matrix.rotate(m_angle, QVector3D(0.0, 0.0, 1.0));
    setTransform(matrix);
}

void Line::draw()
{
    float size = 0.3;
    glColor3f(1.0, 1.0, 1.0);
    //checkError(__LINE__);
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixf(transform().data());
    glBegin(GL_QUADS);
    //ближняя грань
    glVertex3f(0.0, size, size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(0.0, -size, size);
    //левая грань
    glVertex3f(0.0, size, -size);
    glVertex3f(0.0, size, size);
    glVertex3f(0.0, -size, size);
    glVertex3f(0.0, -size, -size);
    // задняя грань
    glVertex3f(0.0, size, -size);
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, -size, -size);
    glVertex3f(0.0, -size, -size);
    //правая грань
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(1.0 * m_length, -size, -size);

    //верх
    glVertex3f(0.0, size, -size);
    glVertex3f(1.0 * m_length, size, -size);
    glVertex3f(1.0 * m_length, size, size);
    glVertex3f(0.0, size, size);
    //низ
    glVertex3f(0.0, -size, -size);
    glVertex3f(1.0 * m_length, -size, -size);
    glVertex3f(1.0 * m_length, -size, size);
    glVertex3f(0.0, -size, size);
    glEnd();
}
