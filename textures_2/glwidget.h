#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QtWidgets>
#include <QGLWidget>
#include "flowchart.h"

class GLWidget : public QGLWidget {
    Q_OBJECT

public:
    explicit GLWidget(QWidget* parent = 0, QGLWidget* shareWidget = 0);
    ~GLWidget();
    void algoritmBlocksLoad(QString src);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void keyPressEvent(QKeyEvent* event);

private:
    QColor clearColor;
    QMatrix4x4 m_projection;
    QMatrix4x4 m_modelTransform;
    QMatrix4x4 m_view;
    QMatrix4x4 m_viewTransform;
    QVector<QVector3D> vertices;
    QVector<QVector2D> texCoords;
    QVector<AlgoritmFlowchart*> m_blocks;
    float aspectRatio;
    QVector3D pos;
    QVector3D center;
};

#endif
