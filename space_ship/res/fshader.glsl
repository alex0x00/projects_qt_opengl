
// параметры источника освещения
uniform vec4 u_light_ambient;
uniform vec4 u_light_diffuse;
uniform vec4 u_light_specular;
uniform vec4 u_light_position;

// параметры для фрагментного шейдера
varying vec2 v_vertex_texcoord;
varying vec3 v_vertex_normal;
varying vec3 v_vertex_lightDir;
varying vec3 v_vertex_viewDir;

// параметры материала
uniform sampler2D u_material_texture;
uniform vec4      u_material_ambient;
uniform vec4      u_material_diffuse;
uniform vec4      u_material_specular;
uniform vec4      u_material_emission;
uniform float     u_material_shininess;


void main(void)
{
    // нормализуем полученные данные для коррекции интерполяции
    vec3 normal   = normalize(v_vertex_normal);
    vec3 lightDir = normalize(v_vertex_lightDir);
    vec3 viewDir  = normalize(v_vertex_viewDir);

    // добавим собственное свечение материала
    vec4 color = u_material_emission;
    // добавим фоновое освещение
    color += u_material_ambient * u_light_ambient;

    // добавим рассеянный свет
    float NdotL = max(dot(normal, lightDir), 0.0);
    color += u_material_diffuse * u_light_diffuse * NdotL;

    // добавим отраженный свет
    float RdotVpow = max(pow(dot(reflect(-lightDir, normal), viewDir), u_material_shininess), 0.0);
    color += u_material_specular * u_light_specular * RdotVpow;

    // вычислим итоговый цвет пикселя на экране с учетом текстуры
    //color *= texture2D(u_material_texture, v_vertex_texcoord);
    //color.r = 1.0;
    gl_FragColor = color;
}

