attribute  vec3 a_position;
attribute  vec3 a_normal;
attribute  vec2 a_texcoord;


// параметры преобразований
uniform mat4 u_transform_model;
uniform mat4 u_transform_viewProjection;
uniform mat4 u_transform_normal;
uniform vec4 u_transform_viewPosition;

// параметры источника освещения
uniform vec4 u_light_ambient;
uniform vec4 u_light_diffuse;
uniform vec4 u_light_specular;
uniform vec4 u_light_position;

// параметры для фрагментного шейдера
varying vec2 v_vertex_texcoord;
varying vec3 v_vertex_normal;
varying vec3 v_vertex_lightDir;
varying vec3 v_vertex_viewDir;

void main(void)
{
    // переведем координаты вершины в мировую систему координат
    vec4 vertex = u_transform_model * vec4(a_position, 1);
//     передадим в фрагментный шейдер некоторые параметры
//     передаем текстурные координаты
    v_vertex_texcoord = a_texcoord;
//     передаем нормаль в мировой системе координат
    v_vertex_normal  = mat3x3(u_transform_normal) * a_normal;
//     передаем направление на источник освещения
    v_vertex_lightDir = vec3(u_light_position);
//     передаем направление от вершины к наблюдателю в мировой системе координат
    v_vertex_viewDir  = vec3(u_transform_viewPosition) - vec3(vertex);

//     переводим координаты вершины в однородные
    gl_Position = u_transform_viewProjection * vertex;
}
