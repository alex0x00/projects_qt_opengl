#include "glwidget.h"
#include <QPainter>
#include <QPaintEngine>

const QString PATH_SRC = ":/";
const QString PATH_SRC_SHIP = ":/ship3.obj";

GLWidget::GLWidget(QWidget* parent)
    : QGLWidget(parent)
{
    frames = 0;
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_NoSystemBackground);
    setAutoBufferSwap(false);

    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(4);
    setFormat(fmt);

    m_model = std::make_unique<Model>(PATH_SRC_SHIP);

    QObject::connect(&timer, SIGNAL(timeout()),
                     this, SLOT(updateGL()));
    timer.setInterval(1);
    timer.start();
}

void GLWidget::resizeGL(int width, int height)
{
    glViewport(0,0, width, height);
    aspectRatio = width / height;
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    if (event->key() == Qt::Key_Escape) {
        QApplication::quit();
    }
}

void GLWidget::initializeGL()
{
    glClearColor(0.01f, 0.05f, 0.1f, 1.0f);

    m_model->setFragmentShaderFile(PATH_SRC + "fshader.glsl");
    m_model->setVertexShaderFile(PATH_SRC + "vshader.glsl");
    m_model->linkShaderProgram();
    m_model->initShaderProgram();

    m_shader = m_model->program();

    GLuint texture;
    auto& storeTextures = m_model->storeTextures();
    QMapIterator<QString, GLuint> i(storeTextures);

    while (i.hasNext()) {
        glGenTextures(1, &texture);
        texture = bindTexture(QImage(i.key()));
        storeTextures[i.key()] = texture;
        qDebug() << "key = " << i.key() << " texture=" << texture;
        qDebug() << "value=" << i.value();
    }
    aspectRatio = width()/height();
    //QMatrix4x4 viewMatrix = m_camera->viewMatrix();
    QMatrix4x4 vp;// = m_camera->projectionMatrix() * viewMatrix;
    vp.perspective(60, aspectRatio, 1, 2);
    m_shader->bind();

    m_shader->setUniformValue("u_transform_viewProjection", vp);
    m_shader->setUniformValue("u_transform_viewPosition", QVector3D());
}

void GLWidget::paintGL()
{
    QPainter painter;
    painter.begin(this);
    painter.beginNativePainting();

    glClearColor(0.01f, 0.05f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //glFrontFace(GL_CW);
    //glCullFace(GL_FRONT);

    glEnable(GL_DEPTH_TEST);
    //прозрачность. настройка алгоритма смешивания
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_DST_ALPHA);
    glEnable(GL_BLEND);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    //glDepthMask(GL_TRUE);
    //glDepthMask(GL_FALSE);

    QMatrix4x4 p, v, vp;
    v.perspective(45, aspectRatio, 10, 100);
    v.lookAt(QVector3D(0, 0, 20), QVector3D(), QVector3D(0, 1, 0));
    vp = p * v;
    m_shader->bind();

    m_shader->setUniformValue("u_transform_viewPosition", QVector3D());

    m_shader->setUniformValue("u_light_ambient",
                              QVector4D(0.1, 0.1, 0.0, 1.0));
    m_shader->setUniformValue("u_light_diffuse",
                              QVector4D(1.0, 1.0, 1.0, 1.0));
    m_shader->setUniformValue("u_light_specular",
                              QVector4D(1.0, 1.0, 1.0, 1.0));
    m_shader->setUniformValue("u_light_position",
                              QVector4D(20.0, 0.0, 20.0, 1.0));

    m_shader->setUniformValue("u_transform_viewProjection", vp);
    m_shader->setUniformValue("u_transform_viewPosition", QVector3D());

    QMatrix4x4 mainModelView;
    mainModelView.setToIdentity();
    modelTransform.rotate(0.5, 0.0, 1.0, 0.0);

    mainModelView = modelTransform;
    mainModelView.scale(3);

    m_model->draw(mainModelView);

    glDisable(GL_DEPTH_TEST);//для вывода fps на текстуре
    //glDisable(GL_CULL_FACE);

    painter.endNativePainting();

    painter.setPen(Qt::red);
    QFont font = painter.font();
    font.setPixelSize(14);
    painter.setFont(font);
    painter.drawText(20, 40,
                     QString("FPS: %1").arg(
                         static_cast<uint>(frames / (time.elapsed() / 1000.0))));
    painter.end();
    swapBuffers();

    if (!(frames % 100)) {
        time.start();
        frames = 0;
    }
    frames++;
}

GLWidget::~GLWidget()
{
}
