TEMPLATE = app
TARGET = SpaceShip

CONFIG += c++14
QT += opengl
LIBS += -lopengl32

INCLUDEPATH  = ../../common3d/include
debug_and_release {
    build_pass:CONFIG(debug, debug|release) {
        LIBS += -L../../common3d/debug
    } else {
        LIBS += -L../../common3d/release
    }
} else {
    LIBS += -L../../common3d/
}
LIBS += -lcommon3d
SOURCES += main.cpp \
    glwidget.cpp
	
HEADERS += glwidget.h

RESOURCES += ../res/res.qrc
