#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QtOpenGL>
#include <QtOpenGL/QGLShaderProgram>
#include <QMatrix4x4>
#include <QTime>
#include <memory>
#include "model.h"

class GLWidget : public QGLWidget {
    Q_OBJECT
public:
    GLWidget(QWidget* parent = 0);
    ~GLWidget();
protected:
    void paintGL();
    void initializeGL();
    void resizeGL(int width, int height);
    void keyPressEvent(QKeyEvent* event);
private:
    float aspectRatio;
    QMatrix4x4 modelTransform;

    QGLShaderProgram* m_shader;
    std::unique_ptr<Model> m_model;

    int frames;
    QTime time;
    QTimer timer;
};
#endif
