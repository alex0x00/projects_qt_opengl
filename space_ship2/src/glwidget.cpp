#include "glwidget.h"
#include <QPainter>
#include <QPaintEngine>
#include <QtCore>

const QString PATH_SRC = ":/";
const QString PATH_SRC_SHIP = ":/ship3.obj";
const QString PATH_SRC_STAR = ":/stars.obj";
const auto SCALE_DISTANCE_STARS = 70;
const auto SCALE_STARS = 0.3f;
const auto COUNT_STARS = 1000;

GLWidget::GLWidget(QWidget* parent)
    : QGLWidget(parent)
{
    setAttribute(Qt::WA_PaintOnScreen);
    setAttribute(Qt::WA_NoSystemBackground);
    setAutoBufferSwap(false);

    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(4);
    setFormat(fmt);
    start();
}

void GLWidget::resizeGL(int width, int height)
{
    glViewport(0,0, width, height);
    aspectRatio = width / height;
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    float step = 1.0f;
    //выходим по escape
    //вращение модели
    switch (event->key()) {
    case Qt::Key_Escape:
        QApplication::quit();
    break;
    case Qt::Key_8:
        m_modelTransform.rotate(step, QVector3D(1, 0, 0));
        break;
    case Qt::Key_2:
        m_modelTransform.rotate(-step, QVector3D(1, 0, 0));
        break;
    case Qt::Key_4:
        m_modelTransform.rotate(step, QVector3D(0, 1, 0));
        break;
    case Qt::Key_6:
        m_modelTransform.rotate(-step, QVector3D(0, 1, 0));
        break;
    case Qt::Key_Up:
        m_viewTransform.rotate(step, QVector3D(1, 0, 0));
        break;
    case Qt::Key_Down:
        m_viewTransform.rotate(-step, QVector3D(1, 0, 0));
        break;
    case Qt::Key_Left:
        m_viewTransform.rotate(step, QVector3D(0, 1, 0));
        break;
    case Qt::Key_Right:
        m_viewTransform.rotate(-step, QVector3D(0, 1, 0));
        break;
    default: return;
    }
    updateGL();//обновляем только на действие пользователя
}

void GLWidget::start(){
    m_model1 = std::make_unique<Model>(PATH_SRC_SHIP);
    m_model2 = std::make_unique<Model>(PATH_SRC_STAR);
    float x, y, z;
    qsrand(QTime::currentTime().elapsed());
    for (int i = 0; i < COUNT_STARS; ++i) {
        x = 0.5f - 2 * ((qrand() % 10000) / 10000.0f);
        y = 0.5f - 2 * ((qrand() % 10000) / 10000.0f);
        z = 0.5f - 2 * ((qrand() % 10000) / 10000.0f);
        QVector3D v(x, y, z);
        if (v.length() < 0.3f) {//отдаление звёзд от корабля
            v.normalize();
        }
        m_stars.push_back(v);
    }
}

void GLWidget::initializeGL()
{
    glClearColor(0.01f, 0.05f, 0.1f, 1.0f);

    m_model1->setFragmentShaderFile(PATH_SRC + "fshader.glsl");
    m_model1->setVertexShaderFile(PATH_SRC + "vshader.glsl");
    m_model1->linkShaderProgram();
    m_model1->initShaderProgram();

    m_shader = m_model1->program();

    m_model2->setProgram(m_shader);

    aspectRatio = width() / height();

    //настройка камеры проекция + положение, взгляд
    QMatrix4x4 vp;
    m_projection.perspective(60, aspectRatio, 10, 1000);
    m_view.lookAt(QVector3D(0, 0, 20), QVector3D(0, 0, 0), QVector3D(0, 1, 0));
    vp = m_projection * m_view;

    m_shader->bind();
    m_shader->setUniformValue("u_transform_viewProjection", vp);
    m_shader->setUniformValue("u_transform_viewPosition", QVector3D());
    //устанавлиаем освещение
    m_shader->setUniformValue("u_light_ambient",
                              QVector4D(0.1f, 0.1f, 0.0f, 1.0f));
    m_shader->setUniformValue("u_light_diffuse",
                              QVector4D(1.0, 1.0, 1.0, 1.0));
    m_shader->setUniformValue("u_light_specular",
                              QVector4D(1.0, 1.0, 1.0, 1.0));
    m_shader->setUniformValue("u_light_position",
                              QVector4D(20.0, 0.0, 20.0, 1.0));
}

void GLWidget::paintGL()
{
    glClearColor(0.01f, 0.05f, 0.1f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);
    //прозрачность. настройка алгоритма смешивания
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_DST_ALPHA);
    glEnable(GL_BLEND);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    //glDepthMask(GL_TRUE);
    //glDepthMask(GL_FALSE);

    QMatrix4x4 mainModelView1, mainModelView2, vp;
    mainModelView1.setToIdentity();
    mainModelView2.setToIdentity();

    vp = m_projection * m_view * m_viewTransform;
    m_shader->bind();
    QVector3D a;
    a = vp * QVector3D();
    m_shader->setUniformValue("u_transform_viewPosition", a);
    m_shader->setUniformValue("u_transform_viewProjection", vp);

    mainModelView1.scale(4.2f);//визуально увеличиваем модель
    mainModelView2.scale(3);
    mainModelView1 = m_modelTransform * mainModelView1;

    m_model1->draw(mainModelView1);

    QVector3D currentStar;
    for (int i = 0; i < m_stars.length(); ++i) {
        currentStar = m_stars[i] * SCALE_DISTANCE_STARS;
        QMatrix4x4 trans;
        trans.translate(currentStar);
        trans.scale(SCALE_STARS);
        m_model2->draw(trans);
    }
    swapBuffers();
}

GLWidget::~GLWidget()
{
}
