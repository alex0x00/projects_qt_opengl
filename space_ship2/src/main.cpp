#include <QApplication>
#include "glwidget.h"

int main( int argc, char ** argv )
{
    QApplication a(argc, argv);

    GLWidget glwidget;
    glwidget.showFullScreen();
    return a.exec();
}
