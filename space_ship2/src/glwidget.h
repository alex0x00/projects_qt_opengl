#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QtOpenGL>
#include <QtOpenGL/QGLShaderProgram>
#include <QMatrix4x4>
#include <QVector3D>
#include <QVector>
#include <memory>
#include "model.h"

class GLWidget : public QGLWidget {
    Q_OBJECT
public:
    GLWidget(QWidget* parent = 0);
    ~GLWidget();
    void start();
protected:
    void paintGL();
    void initializeGL();
    void resizeGL(int width, int height);
    void keyPressEvent(QKeyEvent* event);
private:
    float aspectRatio;
    QMatrix4x4 m_modelTransform;
    QMatrix4x4 m_viewTransform;
    QMatrix4x4 m_projection;
    QMatrix4x4 m_view;
    QGLShaderProgram* m_shader;
    std::unique_ptr<Model> m_model1;
    std::unique_ptr<Model> m_model2;
    QVector<QVector3D> m_stars;
};
#endif
