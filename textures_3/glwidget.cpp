
#include <QtWidgets>
#include <QtOpenGL>

#include "glwidget.h"

GLWidget::GLWidget(QWidget* parent, QGLWidget* shareWidget)
    : QGLWidget(parent, shareWidget)
{
    QGLFormat fmt;
    fmt.setSampleBuffers(true);
    fmt.setSamples(4); //количество пикселей на сглаживание одного
    setFormat(fmt);
    pos = QVector3D(0, 0, 20);
    center = QVector3D(0, 0, 0);
    m_isProjection = true;
    x2 = 20;
    x1 = -x2;

    y2 = 20;
    y1 = -y2;
}

GLWidget::~GLWidget()
{
}

void GLWidget::initializeGL()
{
    //установка значениями
    m_viewTransform.setToIdentity();
    aspectRatio = width() / height();
    m_projection.perspective(60, aspectRatio, 1, 100);

    m_view.lookAt(pos,
                  center,
                  QVector3D(0, 1, 0));

    glEnable(GL_DEPTH_TEST);
}
//метод отрисовки
void GLWidget::paintGL()
{
    glClearColor(0.3, 0.3, 0.3, 1.0); //серый фон
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    //сглаживание вкл
    glEnable(GL_MULTISAMPLE);
    QMatrix4x4 vp;
    //устанавливаем результирующую матрицу проекции
    vp = m_projection * m_viewTransform;

    glMatrixMode(GL_PROJECTION);
    glLoadMatrixf(vp.data()); //передача в opengl
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    //отрисовка объектов
    for (int i = 0; i < m_blocks.length(); ++i) {
        m_blocks[i]->draw();
    }
}

void GLWidget::resizeGL(int width, int height)
{
    //изменяем настройки порта вывода графики
    glViewport(0, 0, width, height);
    aspectRatio = width / height;
    if (m_isProjection) {
        m_projection.setToIdentity();
        m_projection.perspective(60, aspectRatio, 1, 100);
        m_projection = m_projection * m_view;
    }
    updateGL(); //обновляем
}

void GLWidget::keyPressEvent(QKeyEvent* event)
{
    float step = 1.0f;
    //выходи по escape
    //управление камерой перемещение вращение
    if (event->key() == Qt::Key_O) {
        //открывам диалог на выбор файла
        QString fileName = QFileDialog::getOpenFileName(this, tr("Файл алгоритма"),
                                                        "./",
                                                        tr("Text (*.txt *.ini)"));
        algoritmBlocksLoad(fileName);
    } else if (event->key() == Qt::Key_C) {
        m_blocks.clear();
    } else if (event->key() == Qt::Key_F1) {
        //используем перспективную проекцию
        m_isProjection = true;
    } else if (event->key() == Qt::Key_F2) {
        //используем ортографическую проекцию
        m_isProjection = false;
    } else if (event->key() == Qt::Key_Escape) {
        exit(0);
    } else if (event->key() == Qt::Key_8) {
        if (m_isProjection) {
            pos += QVector3D(0.0, -step, 0.0);
            center += QVector3D(0.0, -step, 0.0);
        } else {
            y1 -= step;
            y2 -= step;
        }
    } else if (event->key() == Qt::Key_2) {
        if (m_isProjection) {
            pos += QVector3D(0.0, step, 0.0);
            center += QVector3D(0.0, step, 0.0);
        } else {
            y1 += step;
            y2 += step;
        }
    } else if (event->key() == Qt::Key_4) {
        if (m_isProjection) {
            pos += QVector3D(step, 0.0, 0.0);
            center += QVector3D(step, 0.0, 0.0);
        } else {
            x1 += step;
            x2 += step;
        }
    } else if (event->key() == Qt::Key_6) {
        if (m_isProjection) {
            pos += QVector3D(-step, 0.0, 0.0);
            center += QVector3D(-step, 0.0, 0.0);
        } else {
            x1 -= step;
            x2 -= step;
        }
    } else if (event->key() == Qt::Key_Plus) {

        if (m_isProjection) {
            pos += QVector3D(0.0, 0.0, -step);
        } else {
            x1 *= 1.0 - step / 10;
            x2 *= 1.0 - step / 10;
            y1 *= 1.0 - step / 10;
            y2 *= 1.0 - step / 10;
        }
    } else if (event->key() == Qt::Key_Minus) {
        if (m_isProjection) {
            pos += QVector3D(0.0, 0.0, step);
        } else {
            x1 *= 1.0 + step / 10;
            x2 *= 1.0 + step / 10;
            y1 *= 1.0 + step / 10;
            y2 *= 1.0 + step / 10;
        }
    } else if (event->key() == Qt::Key_Up) {
        m_viewTransform.rotate(-step, QVector3D(1, 0, 0));
    } else if (event->key() == Qt::Key_Down) {
        m_viewTransform.rotate(step, QVector3D(1, 0, 0));
    } else if (event->key() == Qt::Key_Left) {
        m_viewTransform.rotate(-step, QVector3D(0, 1, 0));
    } else if (event->key() == Qt::Key_Right) {
        m_viewTransform.rotate(step, QVector3D(0, 1, 0));
    }
    if (m_isProjection) {
        m_projection.setToIdentity();
        m_projection.perspective(60, aspectRatio, 1, 100);
        m_view.setToIdentity();
        //формируем навую матрицу точки обзора
        m_view.lookAt(pos,
                      center,
                      QVector3D(0, 1, 0));
        m_projection = m_projection * m_view;
    } else {
        m_view.setToIdentity();
        m_projection.setToIdentity();
        m_projection.ortho(x1, x2,
                           y1, y2,
                           -100.0, 100.0);
    }
    updateGL(); //обновляем
}

void GLWidget::algoritmBlocksLoad(QString src)
{
    //загрузка файла с алгоритмом
    QString path_src = src;
    if (!QFile::exists(path_src)) {
        path_src = QCoreApplication::applicationDirPath() + "/" + src;
        if (!QFile::exists(path_src)) {
            qWarning() << "Algoritm file not found:" << path_src;
            return;
        }
    }
    enum AlgoritmBloksFileFormat {
        NONE = 0,
        BLOCK_BEGIN_END,
        BLOCK_ACTION,
        BLOCK_CONDITIONAL,
        BLOCK_INPUT_OUTPUT,
        VECTOR,
        LINE
    } state = NONE;
    QFile file(path_src);
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "Algoritm file not opened:";
    }
    QString line;
    QVector<QString> tokens;
    AlgoritmFlowchart* element = 0;
    QString text;
    QVector2D pos;
    unsigned int length;
    float angle;

    while (!file.atEnd()) {
        line = file.readLine();
        tokens = parseTokens(line);
        if (tokens.length() == 0) {
            continue; //если пустая строка
        }
        if (tokens[0] == "begin") {

            if (tokens[1] == "BeginEnd") {
                state = BLOCK_BEGIN_END;
            } else if (tokens[1] == "Conditional") {
                state = BLOCK_CONDITIONAL;
            } else if (tokens[1] == "Action") {
                state = BLOCK_ACTION;
            } else if (tokens[1] == "InputOutput") {
                state = BLOCK_INPUT_OUTPUT;
            } else if (tokens[1] == "Vector") {
                state = VECTOR;
            } else if (tokens[1] == "Line") {
                state = LINE;
            }
        } else if (tokens[0] == "pos") {
            float x, y;
            x = tokens[1].toFloat();
            y = tokens[2].toFloat();
            pos.setX(x);
            pos.setY(y);
        } else if (tokens[0] == "text") {
            text = line.remove(0, 5); // удаляем text
        } else if (tokens[0] == "angle") {
            angle = tokens[1].toFloat();
        } else if (tokens[0] == "length") {
            length = tokens[1].toUInt();
        } else if (tokens[0] == "end") {
            //описаение объекта завершено, сохраняем
            element = 0;
            switch (state) {
            case NONE:
                break;
            case BLOCK_BEGIN_END:
                element = new BlockBeginEnd(pos, text);
                break;
            case BLOCK_CONDITIONAL:
                element = new BlockConditional(pos, text);
                break;
            case BLOCK_ACTION:
                element = new BlockAction(pos, text);
                break;
            case BLOCK_INPUT_OUTPUT:
                element = new BlockInputOutput(pos, text);
                break;
            case VECTOR:
                element = new Vector(pos, angle);
                break;
            case LINE:
                element = new Line(pos, angle, length);
                break;
            }
            if (element != 0) {
                m_blocks.push_back(element); //добавляем новый элемент
            }
        }
    }
    qDebug() << "Load file algoritm";
}
